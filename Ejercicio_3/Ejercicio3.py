#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import os


# Función para guardar el archivo con cada moficación.
def save_file(aminoacido):

    with open("aminoacidos.json", 'w') as file:
        json.dump(aminoacido, file)


# Función para cargar un archivo.
def load_file():

    with open("aminoacidos.json", 'r') as file:
        aminoacido = json.load(file)

    return aminoacido


# Función para cargar un archivo.
def load_file2():

    # Archivo con los 20 aminoácidos que se usarán para
    # corroborar que los aminoácidos ingresados existan.
    with open("aa_correctos.json", 'r') as file:
        aminoacido = json.load(file)

    return aminoacido


# Función que determina si un elemento está presente en
# un aminoácido o no.
def comparador(aa_con_elemento, elementos):

    i = 0
    aux = 0

    for j in range(len(aa_con_elemento)):

        if(i == 3 and aux == len(elementos)):
            return True

        elif(elementos[i] == aa_con_elemento[j] and i == 0):
            i = i + 1
            aux = aux + 1

        elif(i != 0 and elementos[i] == aa_con_elemento[j]):
            aux = aux + 1
            i = i + 1

        elif(i != 0 and elementos[i] != aa_con_elemento[j]):
            return False

    if(aux == len(elementos)):
        return True

    else:
        return False


# Transforma el diccionario a lista para poder
# trabajar con este.
def transformador(valores, elementos_buscados):

    aa_con_elemento = []
    indices = []

    # Se transforma a una lista
    for i in range(len(valores)):
        aa_con_elemento.append(list(valores[i]))
        elementos = list(elementos_buscados)

    # Realiza la comparación para saber si existe el/los
    # elementos a buscar en los aminoácidos.
    for j in range(len(valores)):
        indices.append(comparador(aa_con_elemento[j], elementos))

    return indices


# Función que busca los aminoácidos ya ingresados a través
# de elementos que poseen en su fórmula química.
def buscador():

    while True:

        try:
            aminoacido = load_file()
            valores = list(aminoacido.values())
            print(valores)
            elementos = input("Ingrese el elemento que desea buscar: ")
            aa_con_elemento = transformador(valores, elementos.upper())

            # Comprobación.
            if(len(aa_con_elemento) == 0):
                print("No existen aminoácidos con ese elemento")

            else:
                i = 0
                print("El/Los aminoácido(s) que posee(n) ese elemento es/son: ")

                # Se recorre todo el diccionario y se imprimen
                # sus "keys" o llaves.
                for key, value in aminoacido.items():
                    if(lista_de_aa[i] == True):
                        print(key)
                    i = i + 1
        except:
            print("\n")
            print("Ha ingresado una opción no válida")
            print("\n")
            buscador()


# Función que busca el aminoácido.
def buscar(aminoacido, texto):

    resultado = aminoacido.get(texto, False)

    return resultado


# Función que comprueba si el aminoácido que
# se ha ingresado existe o no.
def comparacion_aa(aa):

    aa_correcto = load_file2()
    comparacion = aa_correcto.get(aa.upper(), False)

    if(comparacion == False):
        print("\n")
        print("No ha ingresado un aminoácido válido")
        print("Por favor, ingrese un aminoácido válido")
        print("\n")
        agregar()

    else:
        return comparacion


# Función que permite ingresar aminoácidos (Válidos).
def agregar():

    aminoacido = load_file()

    while True:

        print("\n")
        aa = input("Ingrese el aminoácido: ")

        # Comparaciones que se realizan con un archivo que
        # tiene los 20 aminoácidos.
        comparacion_1 = comparacion_aa(aa)
        existe = buscar(aminoacido, aa)

        # Se muestra la fórmula al usuario para que así pueda
        # ingresar el aminoácido correctamente.
        print("La fórmula química es:", comparacion_1)
        aa_correcto = load_file2()
        formula_correcta = aa_correcto[aa.upper()]

        # Serie de comprobaciones para testificar que sí existe
        # el aminoácido ingresado y validar su fórmula química.
        if not existe:

            while True:

                print("\n")
                formula = input("Ingrese la fórmula química del aminoácido: ")

                if(formula.upper() == formula_correcta):

                    temporal = formula.upper()
                    aminoacido[aa] = temporal
                    break

                else:
                    print("Ha ingresado erroneamente la fórmula química del"
                          " aminoácido, por favor hágalo nuevamente")
            break

        else:
            print("\n")
            print("El aminoácido {} ya ha sido ingresado, por favor ingrese"
                  " el aminoácido nuevamente".format(aa))
            agregar()

    # Se guarda el aminoácido ingresado después de la comprobación.
    save_file(aminoacido)


# Función que permite modificar los aminoácido.
def modificar():

    aminoacido = load_file()

    # Ciclo infinito hasta que se ingrese un aminoácido válido.
    while True:

        aa = input("Ingrese el aminoácido que desea modificar: ")
        existe = buscar(aminoacido, aa)

        # Comprueba la existencia del aminoácido, si existe
        # este se puede modificar, sino, no.
        if(existe):

            formula = input("Ingrese la fórmula química del aminoácido: ")
            temporal = {}
            temporal["formula"] = formula
            aminoacido[aa] = temporal
            save_file(aminoacido)

            break

        else:
            print("\n")
            print("El aminoácido ingresado no existe")


# Función que elimina los aminoácido en relación a su nombre.
def eliminar():

    aminoacido = load_file()

    # Ciclo infinito hasta que se ingrese un aminoácido válido.
    while True:

        aa = input("Ingrese el nombre del aminoácido a eliminar: ")
        existe = buscar(aminoacido, aa)

        # Comprueba la existencia del aminoácido, si existe
        # se elimina, sino, no.
        if(existe):
            del aminoacido[aa]
            save_file(aminoacido)
            break

        else:
            print("\n")
            print("El aminoácido ingresado no existe")


# Menú con todas las acciones que tiene el programa.
def menu():

    aminoacido = {}

    while True:
        print("\n")
        print("¿Qué acción desea realizar?")
        print("\n")
        print("A/a: Para agregar un aminoácido")
        print("B/b: Para buscar elementos presentes en los aminoácidos")
        print("M/m: Para modificar un aminoácido")
        print("E/e: Para eliminar un aminoácido")
        print("S/s: Para salir")

        print("\n")
        accion = input("Ingrese la acción a realizar: ")

        if accion.upper() == "A":
            agregar()

        elif accion.upper() == "B":
            buscador()

        elif accion.upper() == "M":
            modificar()

        elif accion.upper() == "E":
            eliminar()

        elif accion.upper() == "S":
            quit()

        else:
            os.system("clear")
            print("La acción que ha realizado no es válida, favor realice"
                  " nuevamente")
            print("\n")
            pass


if __name__ == '__main__':

    menu()

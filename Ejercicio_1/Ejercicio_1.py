#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

# Función que guarda las modificaciones hechas al archivo.
def save_file(aminoacidos):
    with open("aa_correctos.json", 'w') as file:
        json.dump(aminoacidos, file)

# Función que carga el archivo.
def load_file():
    with open("aa_correctos.json", 'r') as file:
        aminoacidos = json.load(file)

    return aminoacidos


def cambiazo():

    aminoacidos = load_file()
    aa_correctos = {}

    # Impresión de los aminoácidos (keys) y sus respectivas
    # fórmulas químicas (values).
    for key, values in aminoacidos.items():
        print(key, values)

    # Cambio de las keys por values y values por keys.
    for key, value in aminoacidos.items():
        aa_correctos[value]= key

    # Impresión de las fórmulas químicas (Ex values) y sus respectivos
    # aminoácidos (Ex keys).
    print("\n")
    for key, values in aa_correctos.items():
        print(key, values)

    save_file(aa_correctos)


if __name__ == "__main__":

    cambiazo()
